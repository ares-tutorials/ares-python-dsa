import timeit, random, json, os
import psutil

class Util():
    def __init__(self) -> None:
        pass
    
    # Time Functions
    def time(self):
        return timeit.timeit()
    
    def executionTime(self, ST, ET):
        return (ET - ST)

    # Random Data Structures
    def getRandomList(self,start=None,end=None,step=None, count=None):
        return random.sample(range(start,end),count)

    # File Ops
    def readJsonFtype(self, filePath):
        obj = None
        try:
            with open(filePath,'r') as file:
                obj = json.load(file)
        except Exception as e:
            e = str(e)
        return obj
    
    def writeJsonResults(self, data = None):
        json_object = None
        writeData = None
        fileName = data.get('program_name')
        filePath = os.path.join(os.getcwd(),f'results/{fileName}.json')
        json_object = self.readJsonFtype(filePath=filePath)
        
        if json_object != None:
            json_object[data.get('run_time')] = data
            writeData = json_object
        else:
            tmp = {}
            tmp[data.get('run_time')] = data
            writeData = tmp
        
        with open(filePath, "w") as file:
            json.dump(writeData, file)
    
    # # inner psutil function
    def process_memory(self):
        process = psutil.Process(os.getpid())
        mem_info = process.memory_info()
        return mem_info.rss
    
    # decorator function
    def profile(self,func):
        def wrapper(*args, **kwargs):
    
            mem_before = self.process_memory()
            result = func(*args, **kwargs)
            mem_after = self.process_memory()
            print("{}:consumed memory: {:,}".format(
                func.__name__,
                mem_before, mem_after, mem_after - mem_before))
    
            return result , {'mem_before':mem_before,'mem_after':mem_after, 'total_memory_consumed':mem_after - mem_before}
        return wrapper