""" Check readme file for Inplace array reversal algorthms meaning."""

class Solution:
    def reverseString(self, s: list[str]) -> None:
        print('Before', s)
        left = 0
        right = len(s) -1
        while left < right:
            print(s[right] , s[left])
            tmpR = s[right]
            tmpL = s[left]
            s[left] = tmpR
            s[right] = tmpL
            right -= 1
            left += 1
        print('After', s)
sol = Solution().reverseString(["H","a","n","n","a","h"])