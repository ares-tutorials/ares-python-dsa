"""
    Solution:
    1. Iterate over all the list elements & compare. but the time complexity will be o(n2)
    2. Have two pointers i, j. if sum(i + j) > target -> reduce j else increase i.
"""


def check_for_target(nums: list, target: int):
    left = 0
    right = len(nums) -1

    while left < right:
        curr = nums[left] + nums[right]
        if curr > target:
            right -= 1
        elif curr < target:
            left += 1
        else:
            return nums[left], nums[right]
print(check_for_target([1,2,4,6,8,9,14,15], 15))