class Solution:
    def numSubarrayProductLessThanK(self, nums: list[int], k: int) -> int:
        lt = ans = 0
        currprod = 1 # multiplication entity. 
        for rt in range(len(nums)):
            currprod *= nums[rt]
            while lt <= rt and currprod >= k:
                currprod //= nums[lt]
                lt += 1 # increment until currprod < k
            ans += rt - lt + 1 # storing ans, each time, after checking currprod < k
        return ans
sol = Solution()
print(sol.numSubarrayProductLessThanK([10, 5, 2, 6],100))