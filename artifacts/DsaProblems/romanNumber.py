class Solution:
    def romanToInt(self, s: str) -> int:
        romanNumberList = {
            "I": 1,
            "V":5,
            "X":10,
            "L":50,
            "C":100,
            "D":500,
            "M":1000
        }
        tmp = []
        tmpNumber = None
        for letter in s:
            
            if romanNumberList.get(letter.upper()) is None:
                return 'Empty String'
            elif isinstance(letter, int):
                return 'Invalid roman Number'
            else:
                num = romanNumberList.get(letter.upper())
                if len(tmp) != 0:
                    lastNum = tmp[-1]
                    if num > lastNum:
                        tmp.pop(-1)
                        num = num - lastNum
                tmp.append(num)
        finalSum = sum(tmp)
        return finalSum

sol = Solution()
print(sol.romanToInt('XXVII'))