from psutil import cpu_count


class Solution():
    def __init__(self) -> None:
        pass
    def solve1(self, nums:list, k:int)-> float:
        lt = 0
        for rt in range(len(nums)):
            # whenever nums[rt] == 0; decrement 1; else decrement 0
            k -= 1 - nums[rt]
            if k < 0:
                # if nums[lt] == 0 ; increment 1 else 0
                k +=1 - nums[lt]
                lt +=1
        return rt -lt +1
sol = Solution()
print(sol.solve1([0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1],3))

"""
Test Cases:
1. ([0,0,1,1,0,0,1,1,1,0,1,1,0,0,0,1,1,1,1],3) : 10
2. ([1,1,1,0,0,0,1,1,1,1,0],2) : 6
"""