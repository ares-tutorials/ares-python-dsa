import numpy as np
import string

"""
    Solutions Ways:
        1. Horizontal Scanning
        2. Vertical Scanning
        3. Divide and conquer
        4. Binary Seach
"""
class Solution:
    def alphaToIdx(self, alpha: str):
        return string.ascii_lowercase.index(alpha) + 1

    def idxToAlpha(self, idx: int):
        return str(chr(ord('`')+idx))

    def symLL(self, dList : list):
        # Find length of maximal list
        maxLenList = len(max(dList, key=len))
        # Make the lists equal in length
        return [ x + [-1] * (maxLenList - len(x)) for x in dList]
        
    def longestCommonPrefix(self, strs: list[str]) -> str:
        print('Input\'s are', strs)
        if len(strs) >= 1 and len(strs) <= 200:
            print('1st constraint check passed')
            bagOfLists = []
            for word in strs:
                if isinstance(word, str):
                    try:
                        wordList = list(self.alphaToIdx(i) for i in word)
                        if len(wordList) >= 0 and len(wordList) <= 200:
                            bagOfLists.append(wordList)
                        else:
                            return 'Failed Constraint #2'
                    except ValueError as Ve:
                        print(str(Ve))
                        return ""
                else:
                    return 'Failed Constraint #3'
            print("bagoflists", bagOfLists)

            # Make lists equal in length
            lst2 = np.array(self.symLL(bagOfLists))
            print('Equal length lists', lst2)

            maxSimilarity = []
            if len(lst2) == 1:
                if len(lst2[0]) != 0:
                    maxSimilarity.append(max(lst2[0]))
            else:
                maxSimilarity = []
                for i in range(len(lst2) -1 ):
                    item = lst2[i+1]
                    tmpHigh = -1
                    for k,v in enumerate(item):
                        if v!= -1 and v == lst2[0][k]:
                            tmpHigh = k
                        else:
                            break
                    maxSimilarity.append(tmpHigh)
            print("maxSimilarity matrix",maxSimilarity)
            if -1 in maxSimilarity:
                return ""
            else:
                if len(maxSimilarity) > 0:
                    minMatch = min(maxSimilarity)
                    minList = (lst2[0])[0:minMatch + 1]
                    word = None
                    for i in minList:
                        if word is not None:
                            word = word + self.idxToAlpha(i)
                        else:
                            word = self.idxToAlpha(i)
                    return word
                else:
                    return ""
        else:
            print('1st constraint failed')
            return ""

sol = Solution()
print(sol.longestCommonPrefix(
["cir","car"]))


# Test Cases 
"""
    ["abc","a"]
    ["a"]
    [""]
    ["1"]
    ["dog","racecar","car"]
    ["cir","car"]
"""