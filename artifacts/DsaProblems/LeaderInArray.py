class FindLeaders():
    def __init__(self) -> None:
        pass

    def process_data(self, dataArr = []):
        try:
            if len(dataArr) == 0:
                raise Exception
            else:
                LeaderList = []
                complexity = {'iIter' :0,'jIter' :0}
                for i in range(len(dataArr)):
                    complexity['iIter'] = i + 1
                    print('Iteration Number', i,'Element is ', dataArr[i])
                    for j in range(len(dataArr) - (i + 1)):
                        complexity['jIter'] = complexity['jIter'] + 1
                        print('Comparing Number with:', dataArr[ j + 1 + i])
                        if(dataArr[i]) >= dataArr[ j + 1 + i]:
                            print('j',j, len(dataArr))
                            if (j + i + 1) == (len(dataArr) - 1):
                                LeaderList.append(dataArr[i])
                            continue
                        else:
                            print('i +1 element was greater. Moving to Next \n')
                            break
                    if i == len(dataArr) - 1:
                        LeaderList.append(dataArr[i])
                return LeaderList, complexity
        except Exception as e:
            print(str(e))
if __name__ == "__main__":
    fl = FindLeaders()
    data = [16, 17, 4, 3, 5, 2]
    print('Given Data is:', data, 'Size of Data', len(data))
    print('List of Leaders are',fl.process_data(dataArr=data))
