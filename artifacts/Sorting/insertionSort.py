# Solution Steps
# 1. For outer loop store value in tmpvariable.
# 2. For inner loop if say j. j > tmpValue; j becomes tmpVariable & j+1 becomes j.
# Time Complexity : O(n2) | Auxiliary Space : O(1)
import random
class Sort():
    def __init__(self) -> None:
        pass

    def swapData(self, data: list, idx: int, value:int):
        data[idx], data[idx] = value , data[idx -1 ]
        return data
         
    def sort(self,data: list):
        print('Length of Input', len(data))

        for i in range(len(data)):
            tmpValue = data[i]
            for j in range(i -1 , -1, -1):
                if data[j] > tmpValue:
                    data[j], data[j+1] = tmpValue, data[j]
                    
        return data
    
sort = Sort()
def getRandomList(start=None,end=None,step=None, count=None):
    return random.sample(range(start,end),count)

randomList = getRandomList(1,10000000000, 2, 10000)
print(sort.sort(data= randomList))
