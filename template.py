import traceback
from utils import utils
from datetime import date, datetime

class Template():
    def __init__(self) -> None:
        self.util = utils.Util()
        
        self.run_time = str((datetime.now()).strftime('%Y%m%d-%H%_M_%S'))

    def executionTracker(self, func):
        def wrapper(*args, **kwargs):

            # **** START EXECUTION ****
            self.sTime = self.util.time()
            self.program_name = func.__name__
            
            #### CALL THE FUNCTION BELOW
            result = None
            @self.util.profile
            def app(func):
                print('l21',func.__name__)
                try:
                    # def wrapper(*args, **kwargs):
                    #     print('l23',func.__name__)
                    result = func()
                    print(result)
                    #     return result(*args, **kwargs)
                    # return wrapper
                except:
                    traceback.format_exc()
            result = app(func)
            
            # **** END EXECUTION ****
            self.eTime = self.util.time()
            self.execution_time = self.util.executionTime(ST=self.sTime, ET=self.eTime)
            data = {
                "program_name" : self.program_name
                ,"execution_date": str(datetime.now())
                ,"execution_start_time": self.sTime
                ,"execution_end_time": self.eTime
                ,"execution_time" : self.execution_time
                ,"run_time":self.run_time
                ,"memory_consumption": result[1]
            }
            self.util.writeJsonResults(data=data)
            return result
        return wrapper