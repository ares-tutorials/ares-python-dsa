*In-place Algorithm*
Algorithm which transforms the input using no auxiliary data structure. However, a small amount of extra storage space is allowed for auxiliary variables.
The input is usually overwritten by the output as the algorithm executes. The algorithm updates its input sequence only through replacement or swapping of elements.